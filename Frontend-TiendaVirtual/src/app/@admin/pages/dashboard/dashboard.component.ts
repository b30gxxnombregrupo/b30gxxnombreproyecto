import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/@services/products.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // Atributos
  formEditar: boolean = false;

  _id: any;
  id: any
  nombre: any;
  cantidad: any;
  categoria: any;
  precio: any;
  urlimg: any;

  productos: any;
  // productos: any [] = [
  //   {
  //     id: 1,
  //     nombre: "Lomo de res",
  //     categoria: "Carnes"
  //   },
  //   {
  //     id: 2,
  //     nombre: "Lechuga",
  //     categoria: "Verduras"
  //   }
  // ]

  constructor(private _producto: ProductsService) { }

  ngOnInit(): void {
    this.obtenerProd();
  }

  // Métodos o Funciones

  editarForm(_id: any, id: any, nombre: any, cantidad: any, categoria: any, precio: any, urlimg: any){
    this.formEditar = true;
    this._id = _id;
    this.id = id;
    this.nombre = nombre;
    this.cantidad = cantidad;
    this.categoria = categoria;
    this.precio = precio;
    this.urlimg = urlimg;
  }

  cancelar(){
    this.formEditar = false;
    this._id = "";
    this.id = "";
    this.nombre = "";
    this.cantidad = "";
    this.categoria = "";
    this.precio = "";
    this.urlimg = "";
  }


  guardarProducto(){
    for(let i = 0; i < 3; i++){
      console.log("i = ", i)
    }
    console.log("Nombre:", this.nombre);
    console.log("Categoría:", this.categoria);

    if(this.nombre != undefined){
      let ind = this.productos.length + 1;

    // variable aux para agregar valores al arreglo
    let prodAux = {
      id: ind,
      nombre: this.nombre,
      cantidad: this.cantidad,
      categoria: this.categoria,
      precio: this.precio,
      urlimg: this.urlimg
    }
    // agrega valores al arreglo
    // this.productos.push(prodAux);
    this._producto.guardarDatos(prodAux)
      .subscribe(datos => {
        console.log("Dato guardado", datos);
        this.obtenerProd();
      })
    }
  }

  actualizarProducto(){
    // variable aux para agregar valores al arreglo
    let prodAux = {
      id: this.id,
      nombre: this.nombre,
      cantidad: this.cantidad,
      categoria: this.categoria,
      precio: this.precio,
      urlimg: this.urlimg
    }

    this._producto.actualizarDatos(this._id, prodAux)
      .subscribe(datos => {
        console.log("Producto actualizado:", datos);
        this.obtenerProd();
      })

    this.formEditar = false;
    this._id = "";
    this.id = "";
    this.nombre = "";
    this.cantidad = "";
    this.categoria = "";
    this.precio = "";
    this.urlimg = "";
  }

  eliminar(indice: any){
    console.log("Dato a eliminar: ", indice);
    // this.productos.splice(indice, 1);
    this._producto.eliminarDatos(indice)
      .subscribe(datos => {
        console.log("Dato eliminado: ", indice);
        this.obtenerProd();
      })
  }

  obtenerProd(){
    this._producto.obtenerDatos()
      .subscribe(datos => {
        this.productos = datos;
        console.log(this.productos);
      })
  }

}
