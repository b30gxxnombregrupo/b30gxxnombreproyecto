// Importar módulo de MongoDB => mongoose
const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    id: Number,
    nombre: String,
    cantidad: String,
    categoria: String,
    precio: String,
    urlimg: String
},
{
    versionKey: false,
    timestamps: true
});

module.exports = mongoose.model('productos', productoSchema);